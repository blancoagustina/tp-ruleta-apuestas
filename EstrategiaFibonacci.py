from Estrategia import Estrategia as Parent
from _operator import pos

class EstrategiaFibonacci(Parent):

    sucesionFibonacci=[]

    def __init__(self,valorInicialApuesta=5,capitalInicial=100000,limiteApuesta=10000):
        super(EstrategiaFibonacci,self).__init__(valorInicialApuesta,capitalInicial,limiteApuesta)

    def fibonacci(self,length):
        a=self.valorInicialApuesta
        b=self.valorInicialApuesta
        sucesion=[a,b]
        for i in range(2,length):
            sucesion.append(sucesion[i-1]+sucesion[i-2])
        return sucesion
    
    def jugar(self,matrizTiradas,capitalInfinito=True,apuestasLimitadas=True):
        self.capitalInfinito=capitalInfinito

        if self.matrizApuestas==[]:
            super(EstrategiaFibonacci,self).jugar(matrizTiradas)
            matrizApuestas=[]
            self.sucesionFibonacci=self.fibonacci(len(matrizTiradas[0]))
            
            for filaGanadora in super(EstrategiaFibonacci,self).matrizGanadora:
                apuestas=[]
                apuestas.append(self.valorInicialApuesta)
                fib=0

                for i in range(len(filaGanadora)-1):
                    if filaGanadora[i]!=0: #si gana se disminuye en dos la posicion en la sucesion
                        if(fib>1): fib-=2
                        else: fib=0
                    else:              #si se pierde se aumenta en uno la posicion en la sucesion
                        fib+=1
                    if self.sucesionFibonacci[fib]<=self.limiteApuesta or not apuestasLimitadas:
                        apuestas.append(self.sucesionFibonacci[fib])
                    else:
                        apuestas.append(self.limiteApuesta)    

                matrizApuestas.append(apuestas)
            
            self.matrizApuestas=matrizApuestas
        return super(EstrategiaFibonacci,self).calcularResultadosNetos()