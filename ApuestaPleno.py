from Apuesta import Apuesta as Parent

class ApuestaPleno(Parent):

    numero=1

    def __init__(self,numero=1):
        self.numero=numero
        super(ApuestaPleno,self).__init__(36,1.0/37) #le paso el factor de ganancia y la a la superclase
        
    def jugar(self,tirada):
        gano=0

        if(tirada==self.numero):
            gano=self.factorGanancia
            
        return gano        