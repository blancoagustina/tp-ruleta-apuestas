from Estrategia import Estrategia as Parent

class EstrategiaParoli(Parent):


    rachaMaximaVictorias=3

    def __init__(self,valorInicialApuesta=5,capitalInicial=100000,limiteApuesta=10000):
        super(EstrategiaParoli,self).__init__(valorInicialApuesta,capitalInicial,limiteApuesta)
    
    def factorApuesta(self):
        return 2      
    
        
    def jugar(self,matrizTiradas,capitalInfinito=True,apuestasLimitadas=True):
        self.capitalInfinito=capitalInfinito

        if self.matrizApuestas==[]:
            super(EstrategiaParoli,self).jugar(matrizTiradas)
            matrizApuestas=[]
            
            for filaGanadora in super(EstrategiaParoli,self).matrizGanadora:
                apuestas=[]
                apuestas.append(self.valorInicialApuesta)
                rachaVictorias=0
                for i in range(len(filaGanadora)-1):
                    if filaGanadora[i]!=0 and rachaVictorias<self.rachaMaximaVictorias:
                        if apuestas[i]*self.factorApuesta()<= self.limiteApuesta or not apuestasLimitadas:
                            apuestas.append(apuestas[i]*self.factorApuesta())
                        else:
                            apuestas.append(self.limiteApuesta)
                        rachaVictorias+=1
                    else:
                        apuestas.append(self.valorInicialApuesta)
                        rachaVictorias=0

                matrizApuestas.append(apuestas)

            self.matrizApuestas=matrizApuestas
        return super(EstrategiaParoli,self).calcularResultadosNetos()
    
        