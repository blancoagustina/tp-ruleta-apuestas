from Apuesta import Apuesta as Parent

class ApuestaParidad(Parent):

    paridad=""

    def __init__(self,paridad="par"):
        self.paridad=paridad
        super(ApuestaParidad,self).__init__(2,18.0/37) #le paso el factor de ganancia y la a la superclase
       
    def jugar(self,tirada):
        gano=0
        
        if tirada!=0 and ((self.paridad=="par" and tirada%2==0)  or (self.paridad=="impar" and tirada%2!=0)): 
            gano=self.factorGanancia

        return gano      