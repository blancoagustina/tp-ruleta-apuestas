from ApuestaColor import ApuestaColor
from copy import copy

class Estrategia():

    valorInicialApuesta=0
    capitalInicial=0
    capitalInfinito=True
    apuesta=ApuestaColor()
    matrizGanadora=[]
    matrizApuestas=[]
    matrizResultados=[]
    limiteApuesta=0
    
    def __init__(self,valorInicialApuesta,capitalInicial,limiteApuesta):
        self.apuesta=ApuestaColor()
        self.valorInicialApuesta=valorInicialApuesta
        self.capitalInicial=capitalInicial
        self.matrizResultados=[]
        self.limiteApuesta=limiteApuesta
    
    def setApuesta(self,apuesta):
        self.apuesta=apuesta
    
    def getMatrizGanadora(self):
        return self.matrizGanadora

    def getCapitalInicial(self):
        valor=0
        if not self.capitalInfinito:
            valor=self.capitalInicial
        return valor

    #recorre cada tirada de cada fila, verificando con el método
    #jugar de cada apuesta, para saber si ganó
    #guarda en filaGanadora los resultados 0/ !=0, gano(factor de ganancia) o no(0)
    #y hace de esas filas una matriz
    #se la devuelve a la estrategia correspondiente para decidir siguiente apuesta
    
    def jugar(self,matrizTiradas):
        if self.matrizGanadora==[]:
            for iteracion in matrizTiradas:
                filaGanadora=[]
                for tirada in iteracion:
                    filaGanadora.append(self.apuesta.jugar(tirada)) #devuelve el factor de ganancia si gana y 0 si pierde
                self.matrizGanadora.append(filaGanadora)

    def calcularResultadosNetos(self):

        self.matrizResultados=[]
        for i in range(len(self.matrizApuestas)):
            filaApuestas=copy(self.matrizApuestas[i])

            resultados=[]
            resultados.append(0)
            if not self.capitalInfinito:
                resultados[0]+=self.capitalInicial
            
            j=0
            while j<(len(filaApuestas)-1) and (resultados[j]>=filaApuestas[j] or self.capitalInfinito):
                factorGanancia = self.matrizGanadora[i][j]
                #factorGanacia-1  porque se descuenta lo apostado
                #si pierde factor de ganancia=0, por lo tanto se resta lo apostado al resultado final
                resultados.append(resultados[j]+filaApuestas[j]*(factorGanancia-1)) 
                j+=1
                
            self.matrizResultados.append(resultados)
        
        return copy(self.matrizResultados)






