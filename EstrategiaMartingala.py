from Estrategia import Estrategia as Parent

class EstrategiaMartingala(Parent):
    
    
    def __init__(self,valorInicialApuesta=5,capitalInicial=100000,limiteApuesta=10000):
        super(EstrategiaMartingala,self).__init__(valorInicialApuesta,capitalInicial,limiteApuesta) 
    
    def factorApuesta(self):
        return 2          #por si es necesario que cambie la multiplicacion en funcion de algo
        
    def jugar(self,matrizTiradas,capitalInfinito=True,apuestasLimitadas=True):
        self.capitalInfinito=capitalInfinito
        
        if self.matrizApuestas==[]:
            super(EstrategiaMartingala,self).jugar(matrizTiradas)
            matrizApuestas=[]
            
            for filaGanadora in super(EstrategiaMartingala,self).matrizGanadora:
                apuestas=[]
                apuestas.append(self.valorInicialApuesta)

                for i in range(1,len(filaGanadora)):
                    if filaGanadora[i-1]!=0: #se gana
                        apuestas.append(self.valorInicialApuesta)
                    elif apuestas[i-1]*self.factorApuesta()<=self.limiteApuesta or not apuestasLimitadas:
                        apuestas.append(apuestas[i-1]*self.factorApuesta())
                    else:
                        apuestas.append(self.limiteApuesta)
                matrizApuestas.append(apuestas)
                
            self.matrizApuestas=matrizApuestas
        return super(EstrategiaMartingala,self).calcularResultadosNetos()
    
    
    
    
    
    
    
    
    
    
    