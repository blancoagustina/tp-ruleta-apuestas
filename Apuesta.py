from abc import ABCMeta

class Apuesta(object):
    __metaclass__ = ABCMeta

    factorGanancia=1
    probabilidadAcierto=1

    def __init__(self,factorGanancia,probabilidadAcierto):
        self.factorGanancia=factorGanancia
        self.probabilidadAcierto=probabilidadAcierto
    
    def getProbabilidadAcierto(self):
        return self.probabilidadAcierto

    def jugar(self,tirada):
        raise NotImplementedError