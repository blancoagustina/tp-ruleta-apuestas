import numpy
import matplotlib.pyplot as ploter
from ApuestaColor import ApuestaColor
from ApuestaParidad import ApuestaParidad
from ApuestaPleno import ApuestaPleno
from EstrategiaMartingala import EstrategiaMartingala
from EstrategiaFibonacci import EstrategiaFibonacci
from EstrategiaParoli import EstrategiaParoli
from Estrategia import Estrategia
from Apuesta import Apuesta

tamanioRuleta=37
cantTiradas=1000
cantIteraciones=10

def graficar(datos,valorEsperado,label,ventana):    
    l1="Simulado"
    l2="Esperado"
    if abs(datos[1])>1:
        l1="Capital"
        l2="Capital Inicial"

    subplot = ventana.add_subplot(tamanio,tamanio,posicionSubPlot)
    subplot.plot(datos, linestyle='-', color='r',label=l1)
    subplot.plot([valorEsperado for _ in range(len(datos))],linestyle='--',color='b',label=l2)
    subplot.set_title(label)
    if posicionSubPlot>2:
        subplot.set_xlabel("Iteraciones")
    ploter.legend(loc="upper right")   
  

def mostrarGrafica():
    ploter.show()
    
    
def switch_estrategia(argument):
    switcher = {
        "1": EstrategiaMartingala(25,3000,1000), #25,10000, 1000
        "2": EstrategiaFibonacci(25,3000,1000),
        "3": EstrategiaParoli(25,3000,1000)
    }
    return switcher.get(argument, None)    
    
    
def switch_apuesta(argument):
    switcher = {
    "1": ApuestaColor("rojo"),
    "2": ApuestaParidad("par"),
    "3": ApuestaPleno(7)   
    }
    return switcher.get(argument,None)    
    

def calcularFAciertosUnaIteracion(flujoCaja):
    fRelativa=[]
    frecuencia=0
    for i in range(1,len(flujoCaja)):
        if flujoCaja[i]>flujoCaja[i-1]:
            frecuencia+=1
        fRelativa.append(float(frecuencia)/(i))
            
    return fRelativa 


def calcularPromedioIntermedio(datos):
    promedioDeDatosEnFuncionDeTiradas=[]
    
    for i in range(len(datos[0])):
        valores=[]
        for j in range(len(datos)):
            valores.append(datos[j][i]) 
        promedioDeDatosEnFuncionDeTiradas.append(numpy.mean(valores))

    return promedioDeDatosEnFuncionDeTiradas   

def calcularFAciertosIteraciones(flujoCaja):
    frecuencias=[]
    for fila in flujoCaja:
        frecuencias.append(calcularFAciertosUnaIteracion(fila))

    longitudes=[]
    max=0
    for fila in frecuencias:
        longitudes.append(len(fila))
        if len(fila)>max:
            max=len(fila)

    fAciertosProm=[]
    for i in range(max):
        cantTiradas=0
        sumaFrec=0
        for j in range(len(frecuencias)):
            if i<longitudes[j]:
                cantTiradas+=1
                sumaFrec+=frecuencias[j][i]               
        fAciertosProm.append(float(sumaFrec)/cantTiradas)

    return fAciertosProm
    

def calcularFlujoCajaIteraciones(flujoCaja):
    longitudes=[]
    max=0
    for fila in flujoCaja:
        longitudes.append(len(fila))
        if len(fila)>max:
            max=len(fila)

    promediosFlujoCaja=[]
    for i in range(max):
        valores=[]
        for j in range(len(flujoCaja)):
            if i<longitudes[j]:
                valores.append(flujoCaja[j][i])
        promediosFlujoCaja.append(numpy.mean(valores))

    return promediosFlujoCaja


resultadositeraciones=numpy.random.randint(0,tamanioRuleta,(cantIteraciones,cantTiradas))
    
apuesta= switch_apuesta(input('¿Qué apuesta desea realizar? 1=Color, 2=Paridad, 3=Pleno'))
estrategia= switch_estrategia(input('¿Qué estrategia desea utilizar? 1=Martingala, 2=Fibonacci, 3=Paroli'))
Estrategia.setApuesta(estrategia,apuesta)

#ventana1
ventanaUnaIteracion = ploter.figure("Una Iteracion")
ventanaUnaIteracion.subplots_adjust(hspace=0.15, wspace=0.15)
tamanio=2
posicionSubPlot=1

flujoCajaCI = estrategia.jugar(resultadositeraciones,capitalInfinito=True,apuestasLimitadas=False)
graficar(flujoCajaCI[0], 0, "Flujo de Caja - Capital Infinito",ventanaUnaIteracion)
posicionSubPlot+=1

frecuenciaAciertosCI = calcularFAciertosUnaIteracion(flujoCajaCI[0])
graficar(frecuenciaAciertosCI, Apuesta.getProbabilidadAcierto(apuesta), "Frecuencia de Aciertos - Capital Infinito",ventanaUnaIteracion)
posicionSubPlot+=1

flujoCajaCA = estrategia.jugar(resultadositeraciones,capitalInfinito=False,apuestasLimitadas=False)
graficar(flujoCajaCA[0], Estrategia.getCapitalInicial(estrategia), "Flujo de Caja - Capital Acotado",ventanaUnaIteracion)
posicionSubPlot+=1

frecuenciaAciertosCA = calcularFAciertosUnaIteracion(flujoCajaCA[0])
graficar(frecuenciaAciertosCA, Apuesta.getProbabilidadAcierto(apuesta), "Frecuencia de Aciertos - Capital Acotado",ventanaUnaIteracion)
posicionSubPlot+=1


#ventana2
ventanaIteraciones = ploter.figure("Iteraciones")
ventanaUnaIteracion.subplots_adjust(hspace=0.7, wspace=0.15)
posicionSubPlot=1

flujoCajaCIIteraciones = calcularPromedioIntermedio(flujoCajaCI)
graficar(flujoCajaCIIteraciones, 0, "Flujo de Caja - Capital Infinito",ventanaIteraciones)
posicionSubPlot+=1

frecuenciaAciertosCIIteraciones = calcularFAciertosIteraciones(flujoCajaCI)
graficar(frecuenciaAciertosCIIteraciones, Apuesta.getProbabilidadAcierto(apuesta), "Frecuencia de Aciertos - Capital Infinito",ventanaIteraciones)
posicionSubPlot+=1

flujoCajaCAIteraciones = calcularFlujoCajaIteraciones(flujoCajaCA)
graficar(flujoCajaCAIteraciones, Estrategia.getCapitalInicial(estrategia), "Flujo de Caja - Capital Acotado",ventanaIteraciones)
posicionSubPlot+=1

frecuenciaAciertosCAIteraciones = calcularFAciertosIteraciones(flujoCajaCA)
graficar(frecuenciaAciertosCAIteraciones, Apuesta.getProbabilidadAcierto(apuesta), "Frecuencia de Aciertos - Capital Acotado",ventanaIteraciones)
posicionSubPlot+=1


#Mostrar todas las ventanas    
mostrarGrafica()



    
