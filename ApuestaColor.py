from Apuesta import Apuesta as Parent

class ApuestaColor(Parent):

    color=""

    def __init__(self,color="negro"):
        self.color=color
        super(ApuestaColor,self).__init__(2,18.0/37) #le paso el factor de ganancia y la a la superclase
        
        
    #Los números negros son aquellos en los que la reducción de la suma de sus dígitos es par.
    #las excepciones son el 10 y el 28, ambos negros.    
    def jugar(self,tirada):
        gano=0  #0=perder, !=0  = ganar
        unidad=tirada%10
        decena=tirada//10

        if tirada!=0 and ((self.color=="rojo" and ((unidad+decena)%2!=0 and tirada!=10 and tirada!=28 )) or 
        (self.color=="negro" and ((unidad+decena)%2==0 or tirada==10 or tirada==28))):
            gano=self.factorGanancia
        
        return gano
            
            
            
            
            
            
            
            
            
            
            
            
            